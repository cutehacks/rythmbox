import QtQuick 2.0
import Box2D 1.0
import QtSensors 5.1
import QtQuick.Particles 2.0
import QtGraphicalEffects 1.0
import "content"
import "content/constants.js" as Constants
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

import Enginio 1.0

import Qt.labs.settings 1.0

Rectangle {
    id: gameCanvas

    width: 1024
    height: 768
    property var ballItem
    property var currentIndex
    property bool running: false
    property int gridSize: 10 * scaleFactor
    property bool moving: false
    property real scaleFactor: 3.0
    property real dpiScale: Screen.pixelDensity / 5.0
    //property var tracker: trackPainter
    property real scaleUnit: Math.max(height, width)/13
    property var enterItem;
    property alias world: worldItem

    color: "#333"
    property color buttonColor: "#444"


    Component {
        id: shareComponent
        SharingPage{}
    }

    StackView {
        id: stack
        anchors.fill: parent

        initialItem: Rectangle {
            color: "#222"

            SoundClip { id: bass ; soundID: "bass" }
            SoundClip { id: snare ; soundID: "snare" }
            SoundClip { id: hihat ; soundID: "hihat" }
            SoundClip { id: splat ; soundID: "click" }
            SoundClip { id: tap ; soundID: "tap" }

            Component {
                id: ballcomp
                Sprite { }
            }


            ScrollView {
                id: scrollView
                x: - (menuPanel.collapsedX - menuPanel.x)
                anchors.top: toolbar.bottom
                anchors.bottom: parent.bottom
                width: parent.width
                flickableItem.interactive: true
                flickableItem.parent.clip: false

                style: ScrollViewStyle {
                    transientScrollBars: true
                    scrollBarBackground: Item { width: 7 ; height: 7}
                    handle: Item {Rectangle { anchors.fill: parent; anchors.margins: 2; radius: 4 ; color: "#33ffffff"}  implicitWidth: 7 ; implicitHeight: 7 }
                }

                World {
                    id: worldItem

                    width: 1024
                    height: 1024

                    Grid {
                        opacity: moving ? 1 : 0
                        Behavior on opacity { NumberAnimation { duration: 500 ; easing.type: Easing.OutCubic} }
                    }

                    gravity: Qt.point(0, 0)

                    EnterItem { }

                    frameTime: 1000/120
                    positionIterations: 5
                    velocityIterations: 5

                    Rectangle {
                        property bool keepAlive: true
                        anchors.fill: parent
                        color: "#ff333333"
                        z: - 1000

                        Rectangle {
                            x: world.width/5
                            y: -world.height
                            height: parent.height * 2
                            rotation: 45
                            width: world.width/4
                            color: "#11000000"
                        }

                        Rectangle {
                            x: 2 * world.width/8
                            y: 0
                            height: parent.height * 2
                            rotation: 45
                            width: world.width/4
                            color: "#11000000"
                        }
                        clip: true
                    }

                    Wall { width: parent.width }
                    Wall { height: parent.height - width; y: width }
                    Wall { height: parent.height - width ; x: parent.width - width ; y: width }
                    Wall { width: parent.width - 2 * height; x: height; y: parent.height - height}
                }
            }

            InstrumentSelector {
                parent: scrollView
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.margins: 40
            }


            CustomToolBar{
                id: toolbar
                anchors.top: parent.top
                width: parent.width
                showMenu: true

                Label{
                    text: "Rythm editor"
                    anchors.centerIn: parent
                    color: Constants.lightGray
                    font.pixelSize: parent.height * 0.4
                    Layout.fillWidth: true
                }

                Image {
                    anchors.left: parent.left
                    anchors.leftMargin: scaleUnit/3
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.height * 0.5
                    height: width
                    fillMode: Image.PreserveAspectFit
                    source: running ? "content/images/stop_icon.png" : "content/images/play_icon.png"
                    smooth: true
                    MouseArea {
                        id: playButtonMouse
                        anchors.fill: parent
                        anchors.margins: -10
                        onPressed: {
                            running = !running
                            if (running) {
                                createBall();
                            } else {
                                ballItem.destroy();
                            }
                        }
                    }
                }

            }

            MenuPanel {
                id: menuPanel
                anchors.top: toolbar.bottom
                anchors.bottom: parent.bottom
            }


            VictoryScreen { id: victoryScreen }

//            TrackGraph {
//                id: trackPainter
//                anchors.top: menuPanel.top
//                anchors.topMargin: 1
//                width: parent.width
//                height: scaleFactor * 2
//                onPercentCorrectChanged: if (percentCorrect === 100 ) { victoryScreen.opacity = 1;}
//            }

            ChoiceDialog {
                id: loadDialog
                model: ListModel { ListElement { title: "Slot 1" } ListElement { title: "Slot 2" } ListElement { title: "Slot 3" }  }//ListElement { title: "Match" } }
                onSelected: load(index)
            }

            ChoiceDialog {
                id: saveDialog
                model: ListModel { ListElement { title: "Slot 1" } ListElement { title: "Slot 2" } ListElement { title: "Slot 3" } }//ListElement { title: "Match" } }
                onSelected: save(index)
            }


            Settings {
                id: appSettings
                property string slot1;
                property string slot2;
                property string slot3;
                property string game;
            }

            EnginioModel {
                id: enginioModel
                client: EnginioClient {
                    id: enginio
                    backendId: "53b55245e5bde52ce7025b9a"
                    onError: console.log("Enginio error:", JSON.stringify(reply.data))
                }
                query: {"objectType": "objects.todos" }
            }

        }
        delegate: StackViewDelegate {
            pushTransition: StackViewTransition {
                function transitionFinished(properties)
                {
                    properties.exitItem.opacity = 1
                }
                PropertyAnimation {
                    target: enterItem
                    property: "x"
                    from: target.width
                    to: 0
                    duration: 500
                    easing.type: Easing.OutCubic
                }
                PropertyAnimation {
                    target: exitItem
                    property: "x"
                    from: 0
                    to: -target.width
                    duration: 500
                    easing.type: Easing.OutCubic
                }
            }
            popTransition: StackViewTransition {
                function transitionFinished(properties)
                {
                    properties.exitItem.opacity = 1
                }
                PropertyAnimation {
                    target: enterItem
                    property: "x"
                    from: -target.width
                    to: 0
                    duration: 500
                    easing.type: Easing.OutCubic
                }
                PropertyAnimation {
                    target: exitItem
                    property: "x"
                    from: 0
                    to: target.width
                    duration: 500
                    easing.type: Easing.OutCubic

                }
            }
            property Component replaceTransition: pushTransition
        }

    }
    property real animStep: 0
    NumberAnimation {
        target: gameCanvas
        property: "animStep"
        duration: 10000
        from: 0
        to: 1000
        running: true
        loops: Animation.Infinite
    }

    onAnimStepChanged: world.nextFrame();

    function snapTo (val, res) { return res * Math.round(val/res); }
    function distance (p1, p2) { return Math.sqrt( Math.pow(p2.y - p1.y, 2) +  Math.pow(p2.x - p1.x, 2)); }


    function resetGame() {
        clear();
        Qt.createQmlObject("import \"content\"; EnterItem{}", world);
//        trackPainter.stop();
//        trackPainter.clearTarget();
    }

    function clearChildren(item) {
        for (var a = item.children.length - 1 ; a >= 0 ; --a) {
            var child = item.children[a]
            if (child["keepAlive"] !== true) {
                child.parent = null
                child.destroy()
            }
        }
    }

    function clear() {
        running = false;
        clearChildren(world);
    }

    function serialize() {
        var data = ""
        for (var i = 0 ; i < world.children.length ; ++i) {
            var item = world.children[i];
            if (item["type"] === undefined)
                continue;
            var string = item.type + "," + item.x + "," + item.y + "," + item.width + "," + item.rotation;
            data = data + string + (i<world.children.length-1 ? ";" : "")
        }
        return data
    }

    function save(index) {
        var data = serialize()
        if (index === 3) {
            appSettings.game = data;
        } else {
            switch (index) {
            case 0: appSettings.slot1 = data; break;
            case 1: appSettings.slot2 = data; break
            case 2: appSettings.slot3 = data; break
            default: print("no such slot");
            }
        }
    }

    function loadData (data) {
        clear ();

        data = data.split(";");
        for (var i = 0 ; i < data.length ; ++i ) {
            var itemdata = data[i].split(",")
            var componentData = "import \"content\";" + itemdata[0] + "{}";
            var item = Qt.createQmlObject(componentData, world);
            item.x = itemdata[1];
            item.y = itemdata[2];
            item.width = itemdata[3];
            item.rotation = itemdata[4];
            if (item.type === "EnterItem")
                gameCanvas.enterItem = item;
        }
    }

    function load (index) {
//        if (index === 3) {
//            trackPainter.load(appSettings.game);
//            return;
//        }
        var data;
        switch (index) {
        case 0: data = appSettings.slot1; break;
        case 1: data = appSettings.slot2; break
        case 2: data = appSettings.slot3; break
        default: print("no such slot");
        }

        if (!data) {
            print("no data!");
            return;
        }

        loadData(data)
    }

    function createBall () {
//        trackPainter.stop();
//        trackPainter.clear();

        if (ballItem)
            ballItem.destroy()

        enterItem.animate()

        ballItem = ballcomp.createObject(world)
        ballItem.x = enterItem.x + enterItem.width/2 - ballItem.width/2
        ballItem.y = enterItem.y + enterItem.height/2 - ballItem.height/2

        ballItem.linearVelocity.x  = 0
        ballItem.linearVelocity.y  = menuPanel.playbackSpeed * scaleFactor
    }

    IntroPage {
        anchors.fill: parent
    }


}

