#ifndef ANDROIDSOUNDPOOL_H
#define ANDROIDSOUNDPOOL_H

#include <QObject>

class AndroidSoundPool : public QObject
{
    Q_OBJECT
public:
    explicit AndroidSoundPool(QObject *parent = 0);
    Q_INVOKABLE void play(const QString &sample);
};


#endif // ANDROIDSOUNDPOOL_H
