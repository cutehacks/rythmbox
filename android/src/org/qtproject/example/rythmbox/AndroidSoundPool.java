/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtAndroidExtras module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

package org.qtproject.example.rythmbox;

//import org.qtproject.example.rythmbox.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.media.SoundPool;
import android.media.AudioManager;
import android.content.Context;
import android.util.Log;
import android.os.Bundle;
import android.view.WindowManager;
import org.qtproject.qt5.android.bindings.QtActivity;

public class AndroidSoundPool extends QtActivity
{
    private static int bassSoundID;
    private static int hihatSoundID;
    private static int snareSoundID;
    private static int clickSoundID;

    private static SoundPool spool;
    private static float volume;
    private static AndroidSoundPool instance;
    private static AudioManager audioManager;

    public AndroidSoundPool() { instance = this; }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        volume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        spool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        bassSoundID = spool.load(instance, R.raw.bass, 1);
        snareSoundID = spool.load(instance, R.raw.snare, 1);
        hihatSoundID = spool.load(instance, R.raw.hihat, 1);
        clickSoundID = spool.load(instance, R.raw.click, 1);
    }

    public static void play(String s) {
        int sampleID = -1;
        if (s.equals("snare"))
            sampleID = snareSoundID;
        else if (s.equals("bass"))
            sampleID = bassSoundID;
        else if (s.equals("hihat"))
            sampleID = hihatSoundID;
        else if (s.equals("click"))
            sampleID = clickSoundID;
        else
            Log.d("AndroidSoundPool", "Invalid sample id: " + s);
        spool.play(sampleID, volume, volume, 1, 0, 1f);
    }
}
