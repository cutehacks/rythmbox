#include "android/src/androidsoundpool.h"
#include <QtAndroidExtras/QAndroidJniObject>

AndroidSoundPool::AndroidSoundPool(QObject *parent)
    : QObject(parent)
{
}

void AndroidSoundPool::play(const QString &sample)
{
    QAndroidJniObject soundString = QAndroidJniObject::fromString(sample);

    QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/rythmbox/AndroidSoundPool",
                                       "play",
                                       "(Ljava/lang/String;)V",
                                       soundString.object<jstring>());
}
