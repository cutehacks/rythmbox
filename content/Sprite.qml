import QtQuick 2.0
import Box2D 1.0
import "constants.js" as Constants

Body {
    id: boxbody
    width: circle.radius * 2;
    height: circle.radius * 2
    z: 10

    PauseAnimation {
        id: explosion
        duration: 100
    }

    fixtures: Circle {
        id: circle
        anchors.centerIn: parent
        radius: 5 * scaleFactor
        density: 0
        friction: 0
        restitution: 1
    }

    Rectangle{
        id: image
        anchors.centerIn: parent
        width: circle.radius * 2
        height: width
        radius: width/2
        color: Constants.darkYellow
        //border.color: Qt.darker(color, 1.4)
        Rectangle {
            x: parent.width/6
            y: parent.height/6
            width: parent.width/2.2
            height: parent.height/2.2
            color: Constants.yellow
            radius: width/2
        }
    }

    parent: world
}
