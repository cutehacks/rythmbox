import QtQuick 2.0
import QtQuick.Layouts 1.0

Item {
    id: root
    implicitWidth: parent.width - 4 * scaleFactor
    implicitHeight: scaleUnit

    signal clicked
    property alias text: label.text
    property color color: buttonColor

    Rectangle {
        id: startButton
        anchors.fill: parent
        border.color: Qt.lighter(color, 1.1)
        border.width: 1//scaleFactor
        color: Qt.darker(root.color, launchMouseArea.pressed ? 1.2 : 1.0)
        radius: root.height / 12
        Text {
            id: label
            anchors.centerIn: parent
            color: "#ccffffff"
            font.family: "Helvetica Neue"
            font.pixelSize: root.height/2.8
            anchors.verticalCenterOffset: 1
        }
        MouseArea {
            id: launchMouseArea
            anchors.fill: parent
            onReleased: splat.play();
            onClicked: { root.clicked() }
        }
        anchors.margins: 4
    }
}
