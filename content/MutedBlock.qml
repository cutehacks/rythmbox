import QtQuick 2.0
import Box2D 1.0
import "constants.js" as Constants

MusicItem {
    id: root
    type: "MutedBlock"

    makeSound: false
    borderColor: Constants.darkGray
    color: Constants.gray
}
