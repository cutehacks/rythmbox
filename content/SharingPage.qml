import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.0
import "constants.js" as Constants
import Enginio 1.0

Rectangle {
    id: cloudDialog
    color: "#333"

    CustomToolBar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right

        Image {
            anchors.left: parent.left
            anchors.leftMargin: parent.height/3
            anchors.verticalCenter: parent.verticalCenter
            width: parent.height/2
            height: width
            fillMode: Image.PreserveAspectFit
            source: "images/back_icon.png"
            smooth: true
            MouseArea {
                id: playButtonMouse
                anchors.fill: parent
                anchors.margins: -16
                onClicked: stack.pop()
            }
        }

        Label{
            text: "Shared designs"
            anchors.centerIn: parent
            color: Constants.lightGray
            font.pixelSize: parent.height/2
            Layout.fillWidth: true
        }
    }

    ColumnLayout {

        anchors.fill: parent
        anchors.topMargin: 0
        anchors.margins: scaleUnit/2


        ListView {
            model: enginioModel
            clip: true
            header: Item {width: 1 ; height: toolbar.height}
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
            delegate: Rectangle {

                color: "#333"
                width: parent.width
                height: scaleUnit * 1.2

                Rectangle {
                    color: "#18ffffff"
                    height: 1
                    width: parent.width
                    anchors.bottom: parent.bottom
                }

                RowLayout {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.right: parent.right
                    spacing: scaleUnit/5
                    Text {
                        color: Constants.lightGray
                        text: model.title
                        font.pixelSize: scaleUnit/3
                    }
                    Text {
                        color: "gray"
                        text: "by " + model.user
                        font.pixelSize: scaleUnit/3
                    }
                    Item {
                        Layout.fillWidth: true

                    }
                    SimpleButton {
                        implicitWidth: scaleUnit * 2
                        text: "Open"
                        //color: Constants.green
                        onClicked: {
                            loadData(model.data)
                            stack.pop()
                        }
                    }
                }
            }
        }
        RowLayout {
            id: row
            spacing: scaleUnit/5
            Label { text: "Title:" ; color: Constants.lightGray ; font.pixelSize: scaleUnit/4}
            TextField { id: titleField  ; Layout.fillWidth: true; font.pixelSize: scaleUnit/4;style: TextFieldStyle { background: Rectangle {color: "#444" ; radius: scaleUnit/8 ; implicitHeight:  scaleUnit * 0.8}  textColor: Constants.lightGray}}
            Label { text: "User:" ; color: Constants.lightGray ; font.pixelSize: scaleUnit/4}
            TextField { id: userField  ; font.pixelSize: scaleUnit/3 ; style: TextFieldStyle { background: Rectangle {color: "#444" ; radius: scaleUnit/8 ; implicitHeight: scaleUnit * 0.8 ; } textColor: Constants.lightGray}}
            SimpleButton {
                implicitWidth: scaleFactor * 2
                enabled: titleField.text && userField.text
                opacity: enabled ? 1 : 0.5
                color: Constants.blue
                text: "Share"
                onClicked: {
                    var jsondata = { title: titleField.text , user: userField.text, data: serialize()}
                    enginioModel.append(jsondata)
//                                    if (enginioModel.rowCount > 0)
//                                        enginioModel.remove(rowCount-1)
                }
            }
        }
    }
}
