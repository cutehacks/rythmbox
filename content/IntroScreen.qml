import QtQuick 2.0
import QtMultimedia 5.0
import Box2D 1.0
import QtSensors 5.1
import QtQuick.Particles 2.0


Rectangle {
    id: introScreen
    Behavior on scale {NumberAnimation {duration: 1000 ;easing.type: Easing.OutCubic}}
    Behavior on opacity {NumberAnimation {duration: 1000 ;easing.type: Easing.OutCubic}}
    color: "black"

    function close () {
        introScreen.scale = 0
        introScreen.opacity = 0
        levelLoader.sourceComponent = levels.level1
        darkIntro.stop()
        music.play()
    }

    Text {
        id: firstLogo
        anchors.centerIn: parent
        font.family: "Helvetica Neue"
        font.weight: Font.Light
        width: 0
        font.pixelSize: 30
        styleColor: "#aaa"
        color: "white"
        text: textItems[count]
        clip: true
        horizontalAlignment: Text.AlignHCenter

        property int count: 0
        property var textItems: [
            "Introducing",
            "A\n Jens Bache-Wiig\ngame",
            "Using Qt 5.2 on iOS",
            "Presented at\nQt Hackathon 2013\nDigia"
        ]

        Timer {
            running: true
            interval: 1500
            onTriggered: numberanim.start()
        }

        SequentialAnimation {
            id: numberanim
            ParallelAnimation {
                NumberAnimation {
                    target: firstLogo
                    property: "width"
                    from: 0
                    to: firstLogo.paintedWidth
                    duration: 500
                    easing.type: Easing.InOutSine

                }
                NumberAnimation {
                    target: firstLogo
                    property: "scale"
                    from: 1
                    to: 1.6
                    duration: 2000
                    easing.type: Easing.OutCubic
                }
                NumberAnimation {
                    target: firstLogo
                    property: "y"
                    from: 0
                    to: 80
                    duration: 2200
                    easing.type: Easing.InOutSine
                }
            }
            NumberAnimation {
                target: firstLogo
                property: "opacity"
                to: 0
                duration: 500 + firstLogo.paintedWidth * 2
                easing.type: Easing.OutCubic
            }
            Component.onCompleted: {
                if (!darkIntro.playing)
                    darkIntro.play()
            }

            onRunningChanged: {if (!running) {
                    firstLogo.count++

                    if (firstLogo.count < firstLogo.textItems.length) {
                        firstLogo.text = firstLogo.textItems[firstLogo.count]
                        firstLogo.font.pixelSize = firstLogo.font.pixelSize// + 12
                        numberanim.restart()
                        firstLogo.opacity = 1
                    } else {
                        darkIntro.stop()
                        introScreen.close()
                    }
                }
            }
        }
        Emitter {
            system: sys
            rotation:  parent.rotation
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            id: particles
            emitRate: firstLogo.width < firstLogo.paintedWidth ? 200 : 0
            lifeSpan: 500
            velocity: AngleDirection {angleVariation: 360; magnitude: 180 ; magnitudeVariation: 100}
            size: 60
            endSize: 10
        }
    }

    ParticleSystem {
        id: sys

        anchors.fill: parent
        running: firstLogo.width > 0 && firstLogo.visible
        ImageParticle {
            color: "#fea"
            source: "images/particle.png"
            anchors.margins: -20
            colorVariation: 0.1
            rotationVelocityVariation: 360
            system: sys
        }
    }
}
