import QtQuick 2.0
import Box2D 1.0
import QtQuick.Particles 2.0

Body {
    id: root

    width: 1.4 * gridSize
    height: width

    property string type: "ExitItem"
    property bool isMoving: false


    Image {
        id: circle
        source: "images/exit_node.png"
        width: parent.width
        height: parent.height
        Rectangle {
            anchors.fill: parent
            radius: width/2
            border.width: 2
            color: "transparent"
            border.color: "#66ffffff"
            antialiasing: true
        }
        Rectangle {
            id: innerCirlce
            anchors.fill: parent
            anchors.margins: 8
            radius: width/2
            opacity: 0
            color: "orange"
        }

        ParallelAnimation {
            id: exitAnim
            NumberAnimation {
                target: innerCirlce
                property: "opacity"
                from: 0.7
                to: 0
                duration: 200
            }
            NumberAnimation {
                target: circle
                property: "scale"
                from: 1.2
                to: 1.0
                duration: 300
                easing.type: Easing.OutCubic
            }
        }
    }

    fixtures: Circle {
        anchors.centerIn: parent
        radius: parent.width/4 * scaleFactor
        density: 0
        friction: 0
        restitution: 1
        sensor: true

        onBeginContact: {
            gameCanvas.createBall();
            exitAnim.restart();
        }
    }

    MouseArea {
        id: dragArea
        anchors.margins: -14 * scaleFactor
        anchors.fill: parent
        drag.target: parent
        onPressed: gameCanvas.moving = true
        onReleased: {
            snapToGrid()
            splat.play()
            gameCanvas.moving = false;
        }
    }


    function snapToGrid () {
        var r = gridSize
        root.x = Math.round(root.x/r) * r
        root.y = Math.round(root.y/r) * r
    }

}
