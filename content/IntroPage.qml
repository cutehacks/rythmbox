import QtQuick 2.0
import "constants.js" as Constants
Rectangle {
    id: root
    color: "#333"

    Text {
        id: presents
        font.family: "Helvetica Neue light"
        font.pixelSize: 56
        color: "White"
        text: "Presents"
        anchors.centerIn: parent
        opacity: 0
    }

    Text {
        font.family: "Helvetica Neue light"
        font.pixelSize: 33
        anchors.horizontalCenter: parent.horizontalCenter
        color: "#44ffffff"
        text: "Tap to continue"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
    }

    Image {
        id: logo
        anchors.horizontalCenterOffset: -20
        anchors.centerIn: parent
        width: implicitWidth/2
        height: implicitHeight/2
        source: "images/logo.png"
        opacity: 0
    }

    Text {
        font.family: "Helvetica Neue light"
        font.pixelSize: 56
        color: Constants.darkYellow
        text: "Drum Box"
        opacity: drumlogo.opacity
        anchors.top: drumlogo.bottom
        anchors.margins: 20
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Image {
        id: drumlogo
        anchors.verticalCenterOffset: -20
        anchors.centerIn: parent
        width: implicitWidth/2
        height: implicitHeight/2
        source: "images/drumlogo.png"
        opacity: 0
    }
    SequentialAnimation {
        id: startAnim
        running: true
        NumberAnimation {
            target: logo
            property: "opacity"
            from: 0 ; to: 1
            duration: 500
        }
        PauseAnimation { duration: 2000 }
        NumberAnimation {
            target: logo
            easing.type: Easing.OutCubic
            property: "opacity"
            from: 1 ; to: 0
            duration: 500
        }
        NumberAnimation {
            target: presents
            property: "opacity"
            from: 0 ; to: 1
            duration: 500
        }
        PauseAnimation { duration: 2000 }
        NumberAnimation {
            target: presents
            property: "opacity"
            easing.type: Easing.OutCubic
            from: 1 ; to: 0
            duration: 500
        }
        NumberAnimation {
            target: drumlogo
            easing.type: Easing.OutCubic
            from: 0 ; to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation { duration: 3000 }
        NumberAnimation {
            target: root
            easing.type: Easing.OutCubic
            property: "opacity"
            from: 1 ; to: 0
            duration: 1000
        }
    }
    MouseArea {
        anchors.fill: parent
        enabled: root.opacity > 0
        onClicked: {
            startAnim.stop();
            root.opacity = 0;
        }
    }
}
