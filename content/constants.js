.pragma library

var yellow = "#f2c500"
var darkYellow = "#f59d00"

var red = "#e94b35"
var darkRed = "#c33824"

var blue = "#2c97de"
var darkBlue = "#227fbb"

var purple = "#227fbb"
var darkPurple = "#9a52b7"

var green = "#1eca6b"
var darkGreen = "#1aaffd"

var cyan = "#00bd9c"
var darkCyan = "#00a185"

var lightGray = "#d9d9d9"
var gray = "#88c9c9c9"
var darkGray = "#88b0b0b0"

var fontSize =  24
var outerMargin = 35
