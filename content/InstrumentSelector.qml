import QtQuick 2.0
import QtQuick.Controls 1.2
import "constants.js" as Constants


Item {
    id: selector

    height: 30 * scaleFactor

    Rectangle {
        anchors.fill: row
        anchors.leftMargin: -40
        anchors.rightMargin: -40
        radius: height/12
        color: "#88555555"
    }

    Row {
        id: row
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: scaleUnit/12
        Component {
            id: basscomp
            BassDrumItem { }
        }

        Component {
            id: snarecomp
            SnareItem { }
        }

        Component {
            id: hihatcomp
            HiHatItem { }
        }

        Component {
            id: exitcomp
            ExitItem { }
        }

        Component {
            id: mutedcomp
            MutedBlock { }
        }

        Repeater {
            model: [
                { component: basscomp , title: "Bass"  },
                { component: snarecomp, title: "Snare"},
                { component: hihatcomp , title: "Hi-Hat" },
                { component: mutedcomp, title: "Reflector" },
                { component: exitcomp , title: "Restart" }]

            Item {
                id: delegate
                width: selector.height
                height: selector.height
                property var item

                function createItem() {
                    item = modelData.component.createObject( itemContainer, {  } )
                    item.x = Math.round((delegate.width - item.width)/2)
                    item.y = Math.round((delegate.height - item.height)/2)
                    animate.restart();
                }

                Item {
                    anchors.fill: parent

                    Item {
                        id: itemContainer
                        Component.onCompleted: createItem()
                        anchors.fill: parent
                        anchors.topMargin: -10
                        anchors.bottomMargin: 10
                        opacity: dragArea.pressed ? 0.7 : 1.0
                    }

                    Text {
                        font.pixelSize: scaleUnit/5
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 4 * scaleFactor
                        color: "#aaffffff"
                        text: modelData.title
                        font.family: "Helvetica Neue"
                    }

                    MouseArea {
                        id: dragArea
                        z: 10
                        property var startPos
                        property bool created: false

                        anchors.fill: parent
                        anchors.margins: -row.spacing/2
                        drag.target: item

                        NumberAnimation {
                            id: animate;
                            target: itemContainer; property: "opacity"
                            from: 0 ; to: 1
                            duration: 200
                            easing.type: Easing.InOutQuad
                        }

                        onClicked: {
                            if (!created && item["soundClip"])
                                item.soundClip.play()
                        }

                        onPressed: {
                            startPos = Qt.point(mouseX, mouseY);
                            gameCanvas.moving = true;
                            item.isMoving = true;
                        }

                        onReleased: {
                            var point = delegate.mapToItem(worldItem, item.x, item.y)
                            if (distance(startPos, Qt.point(mouseX, mouseY)) > 60 &&
                                    worldItem.x < point.x && worldItem.y < point.y &&
                                    point.x < worldItem.width &&  point.y < worldItem.height) {
                                item.parent = world;
                                item.x = point.x;
                                item.y = point.y;
                                created = true;
                                item.isMoving = false;
                                if (created) {
                                    item.snapToGrid();
                                    created = false;
                                    createItem();
                                    splat.play();
                                }
                            } else {
                                item.x = Math.round(delegate.width/2 - item.width/2)
                                item.y = Math.round(delegate.height/2 - item.height/2)
                            }
                            moving = false;
                        }
                    }
                }
            }
        }
    }
}
