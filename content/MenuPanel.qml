import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import "constants.js" as Constants

Rectangle {
    id: toolbar

    Connections {
        target: gameCanvas
        onWidthChanged: {
            animator.enabled = false
            toolbar.trigger()
            animator.enabled = true
        }
    }

    property int expandedWidth: Math.min(gameCanvas.width > gameCanvas.height ?  400 : 0.7 * gameCanvas.width  )
    width: expandedWidth
    color: gameCanvas.color//Qt.lighter(gameCanvas.color, 1.2)
    x: collapsedX
    property int expandedX: gameCanvas.width - width
    property int collapsedX: gameCanvas.width
    Behavior on x { id: animator ; NumberAnimation {duration: 300 ; easing.type: Easing.OutCubic} }
    property bool open: false


    function trigger() {
        if (open)
            toolbar.x = expandedX
        else
            toolbar.x = collapsedX
    }

    onOpenChanged: trigger()

    Rectangle {
        parent: world
        anchors.fill: parent
        color: "#22000000"
        Behavior on opacity {NumberAnimation{}}
        opacity: toolbar.open ? 1 : 0
        property bool keepAlive: true

        MouseArea {
            anchors.fill: parent
            visible: parent.opacity > 0
            onClicked: toolbar.open = false
            drag.target: toolbar
            drag.minimumX: gameCanvas.width - toolbar.width
            onReleased: {
                if (toolbar.x < expandedX + 100 ) {
                    toolbar.open = true
                    toolbar.x = expandedX
                } else {
                    toolbar.open = false
                    toolbar.x = collapsedX
                }
            }
        }
    }

    property alias playbackSpeed: speedSlider.value

    clip: true
    ColumnLayout {
        id: rowLayout
        spacing: 8 * scaleFactor
        anchors.margins: 40
        anchors.fill: parent
        property int offset: 40 - (toolbar.x - expandedX)/4
        anchors.leftMargin: offset
        anchors.rightMargin: -offset + 80

        SimpleButton {
            text: "Share..."
            onClicked: {
                stack.push(shareComponent)
            }
        }

        SimpleButton {
            text: "Load"
            onClicked: { loadDialog.show(); }
        }

        SimpleButton {
            text: "Save"
            onClicked: {  saveDialog.show(); }
        }

        SimpleButton {
            text: "Reset"
            onClicked: gameCanvas.resetGame();
        }

        Label {
            color: Constants.gray
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 18
            text: "Tempo:"
        }

        Slider {
            id: speedSlider
            width: 180
            minimumValue: 400
            maximumValue: 1000
            Layout.fillWidth: true
            value: 600
            style: SliderStyle {
                groove: Rectangle { implicitHeight: 4 * scaleFactor ; color: Qt.darker(gameCanvas.color, 1.2) ; radius: height/2}
                handle: Item {
                    width: gameCanvas.height/18 ; height: width
                    Rectangle { width: gameCanvas.height/18 ; height: width  ; radius: width/2 ; color: "#18000000"  }
                    Rectangle { width: gameCanvas.height/18 ; height: width  ; radius: width/2 ; color:  Qt.lighter(buttonColor, 1.1) ; border.color: Qt.lighter(buttonColor, 1.2) }
                }
            }
        }

        Item { Layout.fillHeight: true }

        SimpleButton {
            text: "Quit"
            onClicked: Qt.quit(0);
            color: Constants.darkRed
        }
    }

    BorderImage {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: 20
        border.bottom: 8
        border.top: 8
        border.left: 8
        border.right: 8
        anchors.margins: -14
        source: "images/dropshadow.png"
    }

}
