import QtQuick 2.0
import Box2D 1.0
import QtQuick.Particles 2.0


Body {
    id: root

    bodyType: Body.Static
    height: 20
    width: 80

    property int duration: 120
    property int easing: Easing.OutElastic

    property string type
    property var oldPos
    property var soundClip

    signal clicked
    signal impact

    property alias color: fillRect.color
    property color borderColor:Qt.lighter(color, 1.2)
    property alias boxFixture: fix

    property bool border: true
    property bool shadow: true
    property bool shakeOnImpact: true

    property int maxLength: gridSize * 15
    property int minLength: gridSize * 2
    property bool sensor : false
    property bool isMoving: false

    property bool makeSound: true
    property bool keepAlive: false

    //    onImpact: if (makeSound) {
    //                  gameCanvas.tracker.startOnFirstImpact()
    //              }

    NumberAnimation on scale {
        id: impactBounce
        from: 0.95
        to: 1.0
        duration: 120
        easing.type: Easing.OutElastic
    }

    z: (angleArea.pressed || isMoving) ? 999 : sensor ? -1 : Math.floor(y/gridSize)

    fixtures: Box {
        id: fix
        anchors.fill: parent
        sensor: root.sensor
        onBeginContact: {
            if (makeSound) {
                impactBounce.restart();
                impact();
                soundClip.play();
            }
        }
        onWidthChanged: { root.invalidateFixture(fix) }
    }

    Item {
        // shadow
        anchors.fill: parent
        rotation: -parent.rotation

        BorderImage {
            source: "images/dropshadow.png"
            border.left: 8
            border.right: 8
            border.top: 8
            border.bottom: 8
            anchors.fill: parent
            anchors.margins: -8
            anchors.topMargin: -4
            anchors.leftMargin: -4
            visible: shadow
            rotation: root.rotation
            opacity: 0.3
        }
    }

    Rectangle {
        id: fillRect
        width: boxFixture.width
        height: boxFixture.height
        border.width: root.border ? 1 : 0
        border.color: borderColor
        color: Qt.lighter("#aacccccc", dragArea.pressed || angleArea.pressed ? 1.2 : 1 )
        antialiasing: true
    }

    //    Text {
    //        text: root.z
    //        anchors.centerIn: parent
    //        font.pixelSize: 8
    //    }

    //    Rectangle {
    //        x: -width/2
    //        y: -width/2
    //        radius: width/2
    //        width: 2 * scaleFactor
    //        height: width
    //        color: fillRect.color
    ////        border.color: fillRect.border.color
    //        //border.width: fillRect.border.width * 1
    //    }

    NumberAnimation on x{
        id: xAnim
        duration: root.duration
        easing.type: root.easing
    }

    NumberAnimation on y{
        id: yAnim
        duration: root.duration
        easing.type: root.easing
    }

    NumberAnimation on width {
        id: widthAnim
        duration: root.duration
        easing.type: root.easing
    }

    function animateRemove() {
        root.transformOrigin = Item.Center
        destroyAnim.start()
    }

    NumberAnimation on scale {
        id: destroyAnim
        duration: 200
        easing.type: Easing.OutCubic
        to: 0
        running: false
        onStopped: root.destroy()
    }

    NumberAnimation on rotation {
        id: rotationAnim
        duration: root.duration
        easing.type: root.easing
    }

    function snapToGrid () {
        var r = gridSize;
        xAnim.to = Math.round(root.x/r) * r;
        yAnim.to = Math.round(root.y/r) * r;
        xAnim.restart();
        yAnim.restart();
    }

    MouseArea {
        id: dragArea
        anchors.margins: -12 // * scaleFactor
        anchors.fill: parent
        drag.target: parent
        onClicked: root.clicked()

        onPressed: {
            gameCanvas.moving = true
            isMoving = true;
        }

        onReleased: {
            snapToGrid()

            if (worldItem.x < root.x && worldItem.y < root.y && root.x < worldItem.width && worldItem.height > root.y) {
                splat.play()
                gameCanvas.moving = false;
                isMoving = false;
            } else {
                root.animateRemove();
            }
        }
        preventStealing: true
    }

    Rectangle {
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        width: root.height
        height: width
        antialiasing: true
        color: borderColor

        MouseArea {
            id: angleArea
            anchors.fill: parent
            anchors.margins: -8* scaleFactor
            preventStealing: true

            onPositionChanged:  {
                if (pressed) {
                    var rotationRes = 15
                    var currLen = snapTo(Math.round(root.width), gridSize)
                    var currAngle = snapTo(Math.round(root.rotation), rotationRes)

                    var p2 = angleArea.mapToItem(worldItem, mouseX, mouseY)
                    var p1 = Qt.point(root.x, root.y)
                    var dx = p2.y - p1.y
                    var dy = p2.x - p1.x
                    var deg = 90 - Math.atan2(dy,  dx) * 180 / Math.PI

                    var len = Math.max(minLength, Math.min(maxLength, distance (p1, p2)));
                    var newLen = snapTo (len, gridSize);
                    var newAngle = snapTo (deg, rotationRes);

                    if (newLen !== currLen && !widthAnim.running) {
                        root.width = newLen
                    } else if (currAngle !== newAngle && !rotationAnim.running) {
                        root.rotation = newAngle
                    }
                }
            }
        }
    }

//    ParticleSystem { id: sys }

//    ImageParticle {
//        source: "images/particle.png"
//        colorVariation: 0.1
//        system: sys
//    }

//    Emitter {
//        id: particles
//        anchors.fill: parent
//        system: sys
//        rotation:  parent.rotation
//        emitRate: 0
//        lifeSpan: 500 * scaleFactor
//        velocity: AngleDirection {
//            angleVariation: 1000 * scaleFactor
//            magnitude: 400 * scaleFactor
//            magnitudeVariation: 200 * scaleFactor
//        }
//        size: 12
//        endSize: 0
//    }

}
