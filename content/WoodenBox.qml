import QtQuick 2.0
import Box2D 1.0

Body {
    id: boxbody
    width: 70
    height: 70
    sleepingAllowed: false
    fixtures: Box {
        density: 8
        categories: Box.Category3
        friction: 3
//        restitution: 0.1
        anchors.fill: parent
        property real delta: 0.0000000000000001
        onContactChanged: {
            if (other.parent.alive && other.categories & Box.Category2 && playTimer.running && Math.abs(linearVelocity.y > 20)) {
                other.parent.alive = false
                other.collidesWith = Box.None
                other.sensor = true
                gameCanvas.splat()
                gameCanvas.score++
            }
        }
    }
    Image {
        anchors.fill: parent
        fillMode: Image.Tile
        Rectangle {
            anchors.fill: parent
            color: "transparent"
            border.color: "#444"
            border.width: 2
            anchors.margins: -1
            radius: 2
        }

        source: "images/wall.JPG"
//        border.color: "gray"
//        color: "lightgray"
        //border.width: 2
        //radius: 2
    }

    parent: world
}
