import QtQuick 2.1

QtObject {


    property Component level1:
        Item {
        property Item background:  Image  {
            parent: backgroundLayer1
            anchors.fill: parent
            fillMode: Image.TileHorizontally
            source: "images/bglayer.png"

            Text {
                x: 3200
                y: 400
                opacity: 0.1
                color: "orange"
                text: "Vote for Jens!!!"
                font.pixelSize: 64
            }
        }

        property Item layer1: Image  {
            parent: backgroundLayer1
            fillMode: Image.TileHorizontally
            anchors.fill: parent
            source: "images/layer1.png"
        }

        property Item layer2: Image  {
            parent: backgroundLayer2
            fillMode: Image.TileHorizontally
            anchors.fill: parent
            source: "images/layer2.png"
        }

        property Item layer3: Image  {
            parent: screen
            fillMode: Image.TileHorizontally
            anchors.fill: parent
            source: "images/layer3.png"
        }

        WoodenBox {
            x: 800 ;     y: 500
            width: 40 ;  height: 160
        }
        WoodenBox {
            y: 500    ;  x: 1000
            width: 40 ;  height: 160
        }
        WoodenBox {
            x: 780 ;     y: 490
            width: 280 ; height: 10
        }
        Pig { x: 900 ; y: 600 }
        Pig { x: 1200 ; y: 600 }

        WoodenBox {
            y: 280
            x: 800 - 20
            height: 10
            width: 240
        }
        WoodenBox {
            y: 300
            x: 800
            height: 200
            width: 40
        }
        WoodenBox {
            y: 300
            x: 800 + 200 - 30
            height: 200
            width: 40
        }
        Pig { x: 690 ; y: 430 }
        Pig { x: 830 ; y: 430 }
        Pig { x: 850 ; y: 430 }
        Pig { x: 830 ; y: 380 }
    }

    property Component level2   :

        Component {
        Item {
            property Item background:  Image  {
                parent: backgroundLayer1
                anchors.fill: parent
                fillMode: Image.TileHorizontally
                source: "images/bglayer-2.png"
            }

            property Item layer1: Image  {
                parent: backgroundLayer1
                fillMode: Image.TileHorizontally
                anchors.fill: parent
                source: "images/layer1.png"
            }

            property Item layer2: Image  {
                parent: backgroundLayer2
                fillMode: Image.TileHorizontally
                anchors.fill: parent
                source: "images/layer2.png"
            }

            property Item layer3: Image  {
                parent: screen
                fillMode: Image.TileHorizontally
                anchors.fill: parent
                source: "images/layer3-2.png"
            }

            WoodenBox {
                x: 820 ;     y: 500
                width: 30 ;  height: 120
            }
            WoodenBox {
                y: 540    ;  x: 1020
                width: 50 ;  height: 120
            }
            WoodenBox {
                y: 540    ;  x: 1250
                width: 50 ;  height: 120
            }
            WoodenBox {
                x: 780 ;     y: 520
                width: 520 ; height: 20
            }

            Pig { x: 900 ; y: 600 }
            Pig { x: 1200 ; y: 600 }
            Pig { x: 900 ; y: 600 }
            Pig { x: 1100 ; y: 600 }
            Pig { x: 1300 ; y: 530 }
            Pig { x: 1100 ; y: 370 }
        }

    }
}
