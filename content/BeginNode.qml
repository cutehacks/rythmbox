import QtQuick 2.0
import Box2D 1.0
import QtQuick.Particles 2.0

Body {
    id: boxbody
    width: circle.radius * 2;
    height: width
    sleepingAllowed: false

    property bool alive: true
//    Component.onCompleted: gameCanvas.count++

    onAliveChanged: if (!alive) {

                        explosion.running = true
                    }

    fixtures: Circle {
        id: circle
        anchors.centerIn: parent
        categories: Box.Category2
        radius: 24
        density: 0.4
        friction: 5
        restitution: 0.1
    }

    Rectangle {
        color: "green"
        anchors.centerIn: parent
        radius: 24
        height: width
        visible: alive || explosion.running
        NumberAnimation on scale{
            id: explosion
            running: false
            from: 1
            to: 0.7
            easing.type: Easing.OutCubic
            duration: 100
        }
    }


    parent: world

}
