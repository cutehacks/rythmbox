import QtQuick 2.0
import Box2D 1.0

Body {
    id: root
    bodyType: Body.Static
    property var oldPos
    property bool keepAlive: true
    width: 0.1// * scaleFactor
    height: 0.1 //* scaleFactor

    Behavior on scale {NumberAnimation {duration: 120 ; easing.type: Easing.OutElastic}}

    fixtures: Box {
        id: fix
        anchors.fill: parent
        categories: Box.Category3
    }

    transformOrigin: Item.Center

//    Rectangle {
//        color: "#22000000"
//        width: parent.width
//        height: parent.height
//        x: 4
//        y: 4
//    }=

//    Item {
//        // shadow
//        anchors.fill: fillRect
//        rotation: -parent.rotation

//        BorderImage {
//            source: "images/dropshadow.png"
//            border.left: 8
//            border.right: 8
//            border.top: 8
//            border.bottom: 8
//            anchors.fill: parent
//            anchors.margins: -8
//            anchors.topMargin: -4
//            anchors.leftMargin: -4
//            rotation: root.rotation
//            opacity: 0.3
//        }
//    }

//    Rectangle {
//        id: fillRect
//        anchors.fill: parent
////        border.color: "#aaffffff"
//        color: "#ff666666"
//        antialiasing: true
//    }
}
