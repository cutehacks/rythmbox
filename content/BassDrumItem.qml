import QtQuick 2.0
import Box2D 1.0
import "constants.js" as Constants

MusicItem {
    id: root
    type: "BassDrumItem"
    //onImpact: trackPainter.createBass()
    soundClip: bass
    color: Constants.red
    borderColor: Constants.darkRed
}
