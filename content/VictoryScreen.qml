import QtQuick 2.0
import "constants.js" as Constants

Rectangle {
    id: victoryScreen

    opacity: 0
    visible: opacity > 0

    Behavior on opacity { NumberAnimation { duration: 100 }}
    anchors.fill: parent
    color: "#33000000"

    Text {
        anchors.centerIn: parent
        text: finishText.text
        font.pixelSize: finishText.font.pixelSize
        color: "#22000000"
        anchors.verticalCenterOffset: 2 * scaleFactor
        anchors.horizontalCenterOffset: 2 * scaleFactor
    }

    Text {
        id: finishText
        anchors.centerIn: parent
        text: "You did it!"
        font.pixelSize: scaleFactor * 28
        color: Constants.blue
    }

    MouseArea {
        anchors.fill: parent
        onClicked: victoryScreen.opacity = 0;
    }
}
