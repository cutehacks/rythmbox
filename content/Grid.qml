import QtQuick 2.0

Item {
    anchors.fill: parent
    property color gridColor: "#09ffffff"
    Row {
        spacing: gridSize - 1
        anchors.fill: parent
        Repeater {
            model: parent.width/gridSize
            Rectangle {
                color: gridColor
                width: 1
                height: parent ? parent.height : 0
            }
        }
    }

    Column {
        spacing: gridSize - 1
        anchors.fill: parent
        Repeater {
            model: parent.height/gridSize
            Rectangle {
                color: gridColor
                height: 1
                width: parent ? parent.width : 0
            }
        }
    }

}
