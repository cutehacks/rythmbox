import QtQuick 2.0
import QtMultimedia 5.0

// Wraps native api on android or QtMultimedia for other platforms

QtObject {
    property string soundID
    property bool isAndroid: Qt.platform.os === "android"
    property var effect: SoundEffect { source: isAndroid ? "" : "sounds/" + soundID + ".wav" }
    function playAndroid() { player.play(soundID) }

    function play() {
        if (isAndroid)
            player.play(soundID)
        else
            effect.play()
    }
}
