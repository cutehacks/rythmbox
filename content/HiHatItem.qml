import QtQuick 2.0
import Box2D 1.0

MusicItem {
    id: root
    soundClip: hihat
    type: "HiHatItem"

    onImpact: {
        fadeAnim.restart()
        //trackPainter.createhHiHat()
    }

    color: "#f1c40f"
    sensor: true
    shadow: false
    border: true

    Rectangle {
        id: flashRect
        anchors.fill: parent
        color: "white"
        opacity: 0
        NumberAnimation on opacity {
            id: fadeAnim
            from: 1
            to: 0
            running: false
            duration: 80
        }
    }
}
