import QtQuick 2.0
import Box2D 1.0
import "constants.js" as Constants

MusicItem {
    id: root
//    onImpact: trackPainter.createSnare()
    soundClip: snare
    color: Constants.cyan
    borderColor: Constants.darkCyan
    type: "SnareItem"
}
