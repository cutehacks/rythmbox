import QtQuick 2.0

Rectangle {
    id: root

    color: "#77000000"
    anchors.fill: parent

    opacity: 0
    visible: opacity > 0

    signal selected (int index)

    property int index: -1

    Behavior on opacity {
        NumberAnimation {}
    }


    function show () {
        root.opacity = 1
        openAnim.restart()
    }

    function close() {
        root.opacity = 0;
    }

    property var model

    MouseArea {
        anchors.fill: parent
        onPressed: close();
    }

    Rectangle {
        anchors.centerIn: parent
        width: 450
        height: 80
        color: "#22ffffff"
        radius: 2
        NumberAnimation on scale {
            id: openAnim
            from: 0
            to: 1
            duration: 250
            easing.type: Easing.OutBounce
        }

        Row {
            id: row
            anchors.fill: parent
            anchors.margins: 8
            spacing: -1
            Repeater {
                model: root.model
                Rectangle {
                    color: "#555"
                    border.color: "#33ffffff"
                    height: parent.height
                    width: parent.width/root.model.count

                    Text {
                        anchors.centerIn: parent
                        text: title
                        font.pixelSize: 20
                        font.family: "Helvetica Neue"
                        color: "#eee"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            root.index = index
                            selected (index);
                            close();
                        }
                    }
                }
            }
        }
    }
}
