import QtQuick 2.0
import QtQuick.Particles 2.0

import "constants.js" as Constants

Item {
    id: trackPainter
    //color: "#11ffffff"

    property real previousWidth: -1
    property bool running: false
    property int index: 0
    property int correct: 0

    property bool matching
    property var matchtable: []
    property int percentCorrect:  matchtable.length > 0 ? Math.round(100 * correct/matchtable.length) : 0

    function startOnFirstImpact() {
        if (!running) {
            clear();
            progressAnimation.restart()
            running = true;
        }
    }

    function stop() {
        running = false;
        clear();
    }

    function clear() {
        clearChildren(track)
        progressRect.width = 0;
        index = 0;
        correct = 0;
    }

    function clearTarget() {
        clearChildren(targetTrack);
        matchtable = [];
        matching = false;
    }

    Item {
        id: track
        width: parent.width
        height: parent.height
    }

    Item {
        id: targetTrack
        anchors.top: track.bottom
        width: parent.width
        height: parent.height
    }

    function save() {
        var data = ""
        for (var i = 0 ; i < track.children.length ; ++i) {
            var item = track.children[i];
            var string = item.type + "," + item.x + "," + item.color;
            data = data + string + (i < track.children.length-1 ? ";" : "")
        }
        return data
    }

    function load(input) {
        matchtable = []
        var data = input.split(";");
        clearChildren(targetTrack)
        for (var i = 0 ; i < data.length ; ++i ) {
            var itemdata = data[i].split(",")
            var item = rectComponent.createObject(targetTrack)
            item.parent = targetTrack
            item.x = itemdata[1];
            item.color = itemdata[2];
            matchtable[i] = {type: item.type, color: item.color, x:item.x}
            matching = true;
        }
    }


    Rectangle {
        id: progressRect
        height: parent.height
        anchors.bottom: parent.bottom
        color: "#08ffffff"
        NumberAnimation on width {
            id: progressAnimation
            from: 0
            to: trackPainter.width
            running: trackPainter.running && gameCanvas.running
            duration: 3000
            onStarted: clearChildren(track)
            loops: Animation.Infinite
        }

        onWidthChanged:  {
            if (width < previousWidth)
                clear();

            previousWidth = width
        }
    }

    Component {
        id: rectComponent
        Rectangle {
            property string type: "Rectangle"
            function bounce() { bounceAnim.restart(); }
            NumberAnimation on scale {
                running: false
                id: bounceAnim
                duration: 250
                from: 4
                to: 1
                easing.type: Easing.OutCubic
            }
            transformOrigin: Item.Center
            height: trackPainter.height
            Component.onCompleted: x = Math.round(progressRect.width)
            width: 4
            opacity: 0.7
        }
    }

    function addNote(item) {
        if (targetTrack.children.length && index < matchtable.length) {
            var match = matchtable[index];
            if (match.x === item.x && match.color === item.color) {
                item.color = "white"
                item.bounce();
                correct++;
            }
        }
        index++
    }

    function createBass() {
        var item = rectComponent.createObject(track)
        item.color = Constants.red
        item.type = "BassItem"
        addNote(item)
    }

    function createSnare() {
        var item = rectComponent.createObject(track)
        item.color = Constants.cyan
        item.type = "SnareItem"
        addNote(item)
    }

    function createhHiHat() {
        var item = rectComponent.createObject(track)
        item.color = Constants.yellow
        item.type = "HiHatItem"
        addNote(item)
    }
}
