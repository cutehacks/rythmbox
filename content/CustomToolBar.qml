import QtQuick 2.0
import QtQuick.Controls 1.2
import "constants.js" as Constants


Item {
    id: selector
    height: scaleUnit
    z: 10
    property bool showMenu: false

    BorderImage {
        opacity: 0.6
        source: "images/dropshadow.png"
        border.left: 8
        border.right: 8
        border.top: 8
        border.bottom: 8
        anchors.fill: parent
        anchors.margins: -4
    }

    Rectangle {
        anchors.fill: parent
        opacity: 0.6
        color: Qt.lighter(gameCanvas.color, 1.6)
    }

    Rectangle {
        height: parent.height
        width: 1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        color: Qt.lighter(gameCanvas.color, 1.4)
    }

    Rectangle {
        height: 1
        width: parent.width
        anchors.bottom: parent.bottom
        color: Qt.darker(gameCanvas.color, 1.4)
    }

    Item {
        width: parent.height * 0.6
        height: parent.height * 0.5
        visible: showMenu
        anchors.right: parent.right
        anchors.rightMargin: scaleUnit/3
        anchors.verticalCenter: parent.verticalCenter
        Column {
            id: iconCol
            anchors.fill: parent
            spacing: parent.height * 0.2
            Repeater { model: 3 ; Rectangle { radius: 4 ; color: "#d4d4d4" ; width: parent.width ; height: parent.height * 0.2 } }
        }
        MouseArea {
            id: menuButtonMouse
            anchors.fill: parent
            anchors.margins: - 8
            onClicked: {
                splat.play()
                menuPanel.open = !menuPanel.open
            }
        }
    }
}
